/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#include "../PixelDefectsEmulatorCondAlg.h"
#include "../PixelDefectsEmulatorAlg.h"
#include "../StripDefectsEmulatorCondAlg.h"
#include "../StripDefectsEmulatorAlg.h"

DECLARE_COMPONENT( InDet::PixelDefectsEmulatorCondAlg )
DECLARE_COMPONENT( InDet::PixelDefectsEmulatorAlg )

DECLARE_COMPONENT( InDet::StripDefectsEmulatorCondAlg )
DECLARE_COMPONENT( InDet::StripDefectsEmulatorAlg )
